package xyz.bowser65.papycraftutils

class RotateStairs /* : CommandExecutor {
    override fun onCommand(sender: CommandSender, command: Command?, label: String?, args: Array<String>): Boolean {
        val stairs = listOf(
                Material.ACACIA_STAIRS,
                Material.BIRCH_STAIRS,
                Material.BRICK_STAIRS,
                Material.COBBLESTONE_STAIRS,
                Material.DARK_OAK_STAIRS,
                Material.DARK_PRISMARINE_STAIRS,
                Material.JUNGLE_STAIRS,
                Material.NETHER_BRICK_STAIRS,
                Material.OAK_STAIRS,
                Material.PRISMARINE_BRICK_STAIRS,
                Material.PRISMARINE_STAIRS,
                Material.PURPUR_STAIRS,
                Material.QUARTZ_STAIRS,
                Material.RED_SANDSTONE_STAIRS,
                Material.SANDSTONE_STAIRS,
                Material.SPRUCE_STAIRS,
                Material.STONE_BRICK_STAIRS
        )
        val directions = mapOf(
                Pair(BlockFace.EAST, BlockFace.NORTH),
                Pair(BlockFace.NORTH, BlockFace.WEST),
                Pair(BlockFace.WEST, BlockFace.SOUTH),
                Pair(BlockFace.SOUTH, BlockFace.EAST)
        )
        if (sender is Player) {
            val player = FaweAPI.wrapPlayer(sender)
            val session = player.newEditSession

            if (player.selection == null) {
                sender.sendMessage("${ChatColor.RED}Your selection is empty!")
                return true
            }
            player.selection ?: return true
            for (pt in FastIterator(player.selection, session)) {
                val block = sender.world.getBlockAt(pt.blockX, pt.blockY, pt.blockZ)
                if (stairs.contains(block.type)) {
                    val data = block.blockData as Stairs
                    data.facing = directions[data.facing]
                    block.blockData = data
                }
            }
        }
        return true
    }
} */
