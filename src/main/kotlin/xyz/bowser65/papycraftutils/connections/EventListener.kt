package xyz.bowser65.papycraftutils.connections

import java.util.Date

import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.inventory.meta.SkullMeta

class EventListener : Listener {

    @EventHandler
    fun onPlayerConnect(e: PlayerJoinEvent) {
        if (!e.player.hasPlayedBefore())
            ConnectionLogs.config.loadConfig()
        ConnectionLogs.players[e.player.uniqueId.toString()] = Date().toInstant().toEpochMilli().toString()
    }

    @EventHandler
    fun onPlayerQuit(e: PlayerQuitEvent) {
        val start = ConnectionLogs.players[e.player.uniqueId.toString()] ?: return
        ConnectionLogs.config.addConnection(e.player, start, Date().toInstant().toEpochMilli().toString())
    }

    @EventHandler
    fun onInventoryClick(e: InventoryClickEvent) {
        if (e.currentItem == null) return

        e.isCancelled = when {
            e.view.title.endsWith("Logs des connexions") -> {
                when {
                    e.currentItem.type == Material.PLAYER_HEAD -> {
                        val meta = e.currentItem.itemMeta as SkullMeta
                        UserInterface.openDetails(e.whoClicked as Player, meta.owningPlayer)
                    }
                    e.currentItem.type == Material.CLOCK -> {
                        UserInterface.openGMT(e.whoClicked as Player)
                    }
                }
                true
            }

            e.view.title.contains("Connexions de ") -> {
                @Suppress("DEPRECATION") // no better alternative, and short-term name storage is not a problem
                when {
                    e.currentItem.itemMeta.displayName.contains("Page suivante") -> {
                        val current = Integer.parseInt(e.clickedInventory.getItem(48).itemMeta.displayName.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1])
                        UserInterface.openDetails(e.whoClicked as Player,
                                Bukkit.getOfflinePlayer(e.clickedInventory.name.split(("${ChatColor.UNDERLINE}").toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]), current)
                    }

                    e.currentItem.itemMeta.displayName.contains("Page précédente") -> {
                        val current = Integer.parseInt(e.clickedInventory.getItem(48).itemMeta.displayName.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1])
                        UserInterface.openDetails(e.whoClicked as Player,
                                Bukkit.getOfflinePlayer(e.clickedInventory.name.split(("${ChatColor.UNDERLINE}").toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1]), current - 2)
                    }

                    e.currentItem.type == Material.SPRUCE_DOOR -> UserInterface.openInventory(e.whoClicked as Player)
                }
                true
            }

            e.view.title.contains("fuseau horaire") -> {
                when {
                    e.currentItem.itemMeta.displayName.contains("+1") -> {
                        if (ConnectionLogs.config.getGMT(e.whoClicked as Player) != 14) {
                            ConnectionLogs.config.setGMT(e.whoClicked as Player, ConnectionLogs.config.getGMT(e.whoClicked as Player) + 1)
                            UserInterface.openGMT(e.whoClicked as Player)
                        }
                    }

                    e.currentItem.itemMeta.displayName.contains("-1") -> {
                        ConnectionLogs.config.setGMT(e.whoClicked as Player, ConnectionLogs.config.getGMT(e.whoClicked as Player) - 1)
                        UserInterface.openGMT(e.whoClicked as Player)
                    }

                    e.currentItem.itemMeta.displayName.contains("Sauvegarder") -> {
                        UserInterface.openInventory(e.whoClicked as Player)
                    }
                }
                true
            }

            else -> false
        }
    }
}
