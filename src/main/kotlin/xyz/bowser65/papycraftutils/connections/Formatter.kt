package xyz.bowser65.papycraftutils.connections

import org.bukkit.ChatColor
import org.bukkit.OfflinePlayer
import org.bukkit.entity.Player
import xyz.bowser65.papycraftutils.toTimeString
import java.text.SimpleDateFormat
import java.time.ZoneId
import java.util.*

object Formatter {
    fun parseDate(to: OfflinePlayer, date: Long): String {
        val config = ConnectionLogs.config

        val format = SimpleDateFormat("dd/MM/yyyy HH:mm:ss z")
        format.timeZone = TimeZone.getTimeZone(ZoneId.of("Etc/" + config.getStringGMT(to)))
        return format.format(Date(date))
    }

    fun getSessionLore(to: Player, player: OfflinePlayer, str: String, isLast: Boolean = false, checkOnline: Boolean = true): MutableList<String> {
        val lore = MutableList(0) { "" }
        var active = ""
        var last = ""

        if (isLast)
            last = "dernière "
        if (checkOnline && player.isOnline)
            active = " ${ChatColor.GREEN}[Active]"

        val start = java.lang.Long.parseLong(str.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0].replace("!r", "").replace("!l", ""))
        var end = java.lang.Long.parseLong(str.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1].replace("!r", "").replace("!l", ""))

        if (end < start) end = Date().toInstant().toEpochMilli()

        lore.add("${ChatColor.GRAY}Détails de la ${last}session$active")
        lore.add(" ")
        lore.add("${ChatColor.GREEN}Connexion le ${parseDate(to, start)}")
        if (str.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0].contains("!r"))
            lore.add("${ChatColor.RED}⚠ Reprise de la connexion après un reload")

        if (!checkOnline || !player.isOnline) {
            lore.add("${ChatColor.RED}Déconnexion le ${parseDate(to, end)}")
            if (str.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[1].contains("!r"))
                lore.add("${ChatColor.RED}⚠ Fin de la connexion due à un reload/reboot")
        }
        lore.add("${ChatColor.GOLD}Durée de la connexion : ${(end - start).toTimeString()}")
        return lore
    }
}
