package xyz.bowser65.papycraftutils.connections

import org.bukkit.ChatColor
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player

class Executor : CommandExecutor {
    override fun onCommand(sender: CommandSender, cmd: Command, label: String, args: Array<String>): Boolean {
        if (sender is Player) {
            if (!sender.hasPermission("papycraft.connections.see")) {
                sender.sendMessage("${ChatColor.RED}Missing permissions (papycraft.connections.see)")
                return true
            }

            UserInterface.openInventory(sender)
        } else {
            sender.sendMessage("hello non-human thing, ur on a wild human zone here...")
        }
        return true
    }
}
