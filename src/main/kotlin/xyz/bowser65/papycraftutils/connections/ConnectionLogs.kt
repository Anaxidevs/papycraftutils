package xyz.bowser65.papycraftutils.connections

import org.bukkit.Bukkit
import xyz.bowser65.papycraftutils.PapycraftUtils
import java.util.*

class ConnectionLogs {
    init {
        config = Database.getInstance(PapycraftUtils.instance)
        config.loadConfig()

        for (player in Bukkit.getOnlinePlayers()) {
            players.put(player.uniqueId.toString(), "${Date().toInstant().toEpochMilli()}!r")
        }
    }

    fun shutdown() {
        for (player in Bukkit.getOnlinePlayers()) {
            config.addConnection(player, players[player.uniqueId.toString()]!!, "${Date().toInstant().toEpochMilli()}!r")
        }
    }

    companion object {
        lateinit var config: Database
            private set

        var players: MutableMap<String, String> = HashMap()
    }
}
