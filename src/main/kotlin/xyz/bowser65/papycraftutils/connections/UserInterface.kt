package xyz.bowser65.papycraftutils.connections

import me.lucko.luckperms.LuckPerms
import me.lucko.luckperms.api.NodeFactory
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Material
import org.bukkit.OfflinePlayer
import org.bukkit.entity.Player
import org.bukkit.event.inventory.InventoryType
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.SkullMeta
import xyz.bowser65.papycraftutils.toTimeString

object UserInterface {
    fun openInventory(to: Player) {
        val inv = Bukkit.createInventory(null, 54, "${ChatColor.GRAY}Logs des connexions")
        getPlayers().forEach { inv.addItem(getInventoryItem(to, it)) }

        val separator = ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1)
        var data = separator.itemMeta
        data.displayName = " "
        separator.itemMeta = data

        val gmt = ItemStack(Material.CLOCK, ConnectionLogs.config.getGMT(to))
        data = separator.itemMeta
        data.displayName = "${ChatColor.RED}Paramètres du fuseau horaire"
        gmt.itemMeta = data

        for (i in 0..51) {
            if (inv.getItem(i) == null)
                inv.setItem(i, separator)
        }

        inv.setItem(52, separator)
        inv.setItem(53, gmt)

        to.openInventory(inv)
    }

    fun openDetails(player: Player, who: OfflinePlayer, seek: Int = 0) {
        val inv = Bukkit.createInventory(null, 54, "${ChatColor.GRAY}Connexions de ${ChatColor.UNDERLINE}${who.name}")

        val list = ConnectionLogs.config.getConnections(who)
        if (who.isOnline)
            list.add(ConnectionLogs.players[player.uniqueId.toString()] + ":42")

        list.reverse()
        val it = list.iterator()
        var hasNextPage = true

        for (i in 0 until seek * 36) {
            it.next()
        }

        for (i in 0..35) {
            if (it.hasNext()) {
                val connection = ItemStack(Material.BOOK, 1)
                val data = connection.itemMeta
                data.displayName = "${ChatColor.GRAY}Connexion #${list.size - (seek * 36 + i)}"
                data.lore = Formatter.getSessionLore(player, who, it.next(), false, i == 0 && seek == 0)
                connection.itemMeta = data
                inv.setItem(i, connection)
            } else {
                hasNextPage = false
            }
        }

        val separator = ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1)
        var data = separator.itemMeta
        data.displayName = " "
        separator.itemMeta = data

        val next = ItemStack(Material.ARROW, 1)
        data = next.itemMeta
        data.displayName = "${ChatColor.GREEN}Page suivante"
        next.itemMeta = data

        val prev = ItemStack(Material.ARROW, 1)
        data = prev.itemMeta
        data.displayName = "${ChatColor.RED}Page précédente"
        prev.itemMeta = data

        val page = ItemStack(Material.PAPER, 1)
        data = page.itemMeta
        data.displayName = "${ChatColor.GRAY}Page ${seek + 1} sur ${ConnectionLogs.config.getConnections(who).size / 36 + 1}"
        page.itemMeta = data

        val home = ItemStack(Material.SPRUCE_DOOR, 1)
        data = home.itemMeta
        data.displayName = "${ChatColor.GOLD}Retour"
        home.itemMeta = data

        val gmt = ItemStack(Material.CLOCK, Math.max(1, ConnectionLogs.config.getGMT(player)))
        data = separator.itemMeta
        data.displayName = "${ChatColor.RED}Paramètres du fuseau horaire"
        gmt.itemMeta = data

        for (i in 0..8) {
            inv.setItem(36 + i, separator)
        }

        if (seek == 0)
            inv.setItem(45, separator)
        else
            inv.setItem(45, prev)
        inv.setItem(46, separator)
        inv.setItem(47, separator)
        inv.setItem(48, page)
        inv.setItem(49, home)
        inv.setItem(50, gmt)
        inv.setItem(51, separator)
        inv.setItem(52, separator)
        if (hasNextPage)
            inv.setItem(53, next)
        else
            inv.setItem(53, separator)

        player.openInventory(inv)
    }

    fun openGMT(player: Player) {
        val inv = Bukkit.createInventory(null, InventoryType.HOPPER, "${ChatColor.GRAY}Paramètres du fuseau horaire")

        val separator = ItemStack(Material.GRAY_STAINED_GLASS_PANE, 1)
        var data = separator.itemMeta
        data.displayName = " "
        separator.itemMeta = data

        val add = ItemStack(Material.GREEN_STAINED_GLASS_PANE, 1)
        data = add.itemMeta
        data.displayName = "${ChatColor.GREEN}+1"
        add.itemMeta = data

        val gmt = ItemStack(Material.CLOCK, Math.max(1, ConnectionLogs.config.getGMT(player)))
        data = gmt.itemMeta
        data.displayName = "${ChatColor.GRAY}${ConnectionLogs.config.getStringGMT(player)}"
        gmt.itemMeta = data

        val remove = ItemStack(Material.RED_STAINED_GLASS_PANE, 1)
        data = remove.itemMeta
        data.displayName = "${ChatColor.RED}-1"
        remove.itemMeta = data

        val ok = ItemStack(Material.SLIME_BALL, 1)
        data = ok.itemMeta
        data.displayName = "${ChatColor.GREEN}Sauvegarder"
        ok.itemMeta = data

        inv.setItem(0, remove)
        inv.setItem(1, gmt)
        inv.setItem(2, add)
        inv.setItem(3, separator)
        inv.setItem(4, ok)

        player.openInventory(inv)
    }

    private fun getPlayers(): List<OfflinePlayer> {
        val node = LuckPerms.getApi().nodeFactory.newBuilder("papycraft.connections.log").build()
        return Bukkit.getServer().offlinePlayers.filter {
            LuckPerms.getApi().getUser(it.uniqueId)?.hasPermission(node)?.asBoolean() ?: false
        }
    }

    private fun getInventoryItem(to: Player, player: OfflinePlayer): ItemStack {
        val lore: MutableList<String>
        if (ConnectionLogs.config.getConnections(player).size == 0) {
            lore = ArrayList()
            lore.add("${ChatColor.GRAY}Aucune connexion enregistrée")
        } else {
            val data = if (player.isOnline) "${ConnectionLogs.players[player.uniqueId.toString()]}:42" else ConnectionLogs.config.getConnections(player)[0]
            val last = if (player.isOnline) ConnectionLogs.players[player.uniqueId.toString()]?.replace("!r", "")?.replace("!l", "")?.toLong()
                    ?: 0 else player.lastPlayed

            lore = Formatter.getSessionLore(to, player, data, true)
            lore.add(" ")
            lore.add("${ChatColor.GOLD}Première connexion le ${Formatter.parseDate(to, player.firstPlayed)}")
            lore.add("${ChatColor.GREEN}Dernière connexion le ${Formatter.parseDate(to, last)}")
            lore.add("${ChatColor.LIGHT_PURPLE}Temps total de jeu : ${ConnectionLogs.config.getTotalPlayed(player).toTimeString()}")
        }

        val skull = ItemStack(Material.PLAYER_HEAD, 1)
        val skullMeta = skull.itemMeta as SkullMeta
        skullMeta.owningPlayer = player
        skullMeta.displayName = "${ChatColor.BLUE}${player.name}"
        skullMeta.lore = lore
        skull.itemMeta = skullMeta
        return skull
    }
}
