package xyz.bowser65.papycraftutils.connections

import java.io.File
import java.io.IOException
import java.util.ArrayList
import java.util.Date
import java.util.HashMap

import org.bukkit.Bukkit
import org.bukkit.OfflinePlayer
import org.bukkit.configuration.InvalidConfigurationException
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.plugin.java.JavaPlugin

// @todo: Migrate to SQL-based storage (see #4)
class Database private constructor(private val plugin: JavaPlugin) {
    private val configs = HashMap<String, YamlConfiguration>()

    fun loadConfig() {
        /* Basic config */
        try {
            for (player in Bukkit.getWhitelistedPlayers()) {
                configs[player.uniqueId.toString()] = loadPlayerConfig(player)
            }
        } catch (ex: Exception) {
            plugin.logger.severe("Fatal error when loading configs : $ex")
        }
    }

    @Throws(IOException::class, InvalidConfigurationException::class)
    fun loadPlayerConfig(player: OfflinePlayer): YamlConfiguration {
        if (configs.containsKey(player.uniqueId.toString()))
            return configs[player.uniqueId.toString()]!!

        val conf = File(plugin.dataFolder, "${player.uniqueId}.yml")

        val config = YamlConfiguration()
        if (!plugin.dataFolder.exists()) {
            plugin.dataFolder.mkdirs()
        }
        if (!conf.exists()) {
            conf.createNewFile()
        }
        val list = ArrayList<String>()

        config.load(conf)
        config.addDefault("total", 0) // We can't check how long did the player played before
        config.addDefault("connections", list)
        config.addDefault("GMT", 2)
        config.options().copyDefaults(true)
        config.save(conf)

        return config
    }

    fun getTotalPlayed(player: OfflinePlayer): Long {
        val playerId = player.uniqueId.toString()
        var modifier: Long = 0
        if (ConnectionLogs.players.containsKey(playerId)) {
            modifier = Date().toInstant().toEpochMilli() - (ConnectionLogs.players[playerId]?.replace("!r", "")?.replace("!l", "")?.toLong()
                    ?: 0)
        }
        return configs[playerId]!!.getLong("total") + modifier
    }

    fun getConnections(player: OfflinePlayer): MutableList<String> {
        return configs[player.uniqueId.toString()]?.getStringList("connections") ?: MutableList(0) { "" }
    }

    fun getGMT(player: OfflinePlayer): Int {
        return configs[player.uniqueId.toString()]?.getInt("GMT") ?: 2
    }

    fun getStringGMT(player: OfflinePlayer): String {
        val gmt = getGMT(player)
        if (gmt > 0) return "GMT+" + Math.abs(gmt)
        return if (gmt < 0)
            "GMT-" + Math.abs(gmt)
        else
            "GMT"
    }

    fun addTimePlayed(player: OfflinePlayer, played: Long) {
        val playerId = player.uniqueId.toString()

        configs[playerId]?.set("total", getTotalPlayed(player) + played)
        val conf = File(plugin.dataFolder, "$playerId.yml")
        try {
            configs[playerId]?.save(conf)
        } catch (e: IOException) {
            // shouldn't happen, let it fail silently
        }
    }

    fun addConnection(player: OfflinePlayer, login: String, logout: String) {
        val playerId = player.uniqueId.toString()

        if (!configs.containsKey(playerId)) {
            return
        }

        val list = getConnections(player)
        list.add("$login:$logout")
        configs[playerId]?.set("connections", list)
        val conf = File(plugin.dataFolder, "$playerId.yml")

        try {
            configs[playerId]?.save(conf)
        } catch (e: IOException) {
            // shouldn't happen, let it fail silently
        }

        addTimePlayed(player, logout.replace("!r", "").replace("!l", "").toLong() - login.replace("!r", "").replace("!l", "").toLong())
    }

    fun setGMT(player: OfflinePlayer, GMT: Int) {
        val playerId = player.uniqueId.toString()

        configs[playerId]?.set("GMT", GMT)
        val conf = File(plugin.dataFolder, player.uniqueId.toString() + ".yml")

        try {
            configs[playerId]?.save(conf)
        } catch (e: IOException) {
            // shouldn't happen, let it fail silently
        }

    }

    // Singleton
    companion object {
        private var instance: Database? = null

        fun getInstance(plugin: JavaPlugin): Database {
            if (instance == null) {
                instance = Database(plugin)
            }
            return instance!!
        }
    }
}
