package xyz.bowser65.papycraftutils

import com.google.common.io.ByteStreams
import org.bukkit.Bukkit
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.command.BlockCommandSender
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.event.Listener
import org.bukkit.plugin.java.JavaPlugin
import xyz.bowser65.papycraftutils.connections.ConnectionLogs
import xyz.bowser65.papycraftutils.connections.EventListener
import xyz.bowser65.papycraftutils.connections.Executor
import sun.audio.AudioPlayer.player


class PapycraftUtils : JavaPlugin(), Listener, CommandExecutor {
    companion object {
        lateinit var logs: ConnectionLogs
        lateinit var instance: PapycraftUtils
            private set
    }

    override fun onEnable() {
        instance = this
        logs = ConnectionLogs()

        val gilet = GiletJaune()
        this.server.messenger.registerOutgoingPluginChannel(this, "BungeeCord")
        Bukkit.getPluginManager().registerEvents(EventListener(), this)
        Bukkit.getPluginManager().registerEvents(gilet, this)
        Bukkit.getPluginCommand("connections").executor = Executor()
        // Bukkit.getPluginCommand("/rotatestairs").executor = RotateStairs()
        Bukkit.getPluginCommand("/gilet").executor = gilet
        Bukkit.getPluginCommand("/crs").executor = gilet
        Bukkit.getPluginCommand("zyva").executor = this
        logger.info { "PapycraftUtils started!" }
    }

    override fun onDisable() {
        logs.shutdown()
        logger.info { "PapycraftUtils stopped" }
    }

    override fun onCommand(sender: CommandSender, command: Command?, label: String?, args: Array<String>): Boolean {
        when (sender) {
            is Player -> {
                if (!sender.hasPermission("papycraft.zyva")) {
                    sender.sendMessage("${ChatColor.RED}Missing permissions (papycraft.zyva)")
                    return true
                }

                @Suppress("UnstableApiUsage")
                val out = ByteStreams.newDataOutput()

                val server = args[0]
                out.writeUTF("Connect")
                out.writeUTF(server)
                sender.sendPluginMessage(this, "BungeeCord", out.toByteArray())
            }
            is BlockCommandSender -> {
                val player = getNearestPlayer(sender.block.location)
                if (player != null) {
                    if (player.location.distance(sender.block.location) < 25) {
                        @Suppress("UnstableApiUsage")
                        val out = ByteStreams.newDataOutput()

                        val server = args[0]
                        out.writeUTF("Connect")
                        out.writeUTF(server)
                        player.sendPluginMessage(this, "BungeeCord", out.toByteArray())
                    }
                }
            }
            else -> sender.sendMessage("hello non-human thing, ur on a wild human zone here...")
        }
        return true
    }

    private fun getNearestPlayer(loc: Location): Player? {
        var result: Player? = null
        var lastDistance = java.lang.Double.MAX_VALUE
        for (p in loc.world.players) {
            val distance = loc.distance(p.location)
            if (distance < lastDistance) {
                lastDistance = distance
                result = p
            }
        }

        return result
    }
}
