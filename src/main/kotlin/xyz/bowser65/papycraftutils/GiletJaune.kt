package xyz.bowser65.papycraftutils

import org.bukkit.ChatColor
import org.bukkit.Color
import org.bukkit.Color.fromRGB
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.command.Command
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender
import org.bukkit.enchantments.Enchantment
import org.bukkit.entity.*
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntitySpawnEvent
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.inventory.ItemStack
import org.bukkit.inventory.meta.LeatherArmorMeta
import java.util.HashSet

class GiletJaune : Listener, CommandExecutor {

    @EventHandler
    fun onPlayerJoin(e: PlayerJoinEvent) {
        if (e.player.hasPermission("papycraft.gilet")) {
            applyGilet(e.player)
        }
    }

    @EventHandler
    fun onPlayerSpawn(e: EntitySpawnEvent) {
        if (e.entityType == EntityType.PLAYER && e.entity.hasPermission("papycraft.gilet")) {
            applyGilet(e.entity as Player)
        }
    }

    @EventHandler
    fun onInventoryEvent(e: InventoryClickEvent) {
        if (e.view.player is Player && e.view.player.hasPermission("papycraft.gilet")) {
            applyGilet(e.view.player)
        }
    }

    override fun onCommand(sender: CommandSender, command: Command, label: String?, args: Array<String>): Boolean {
        if (sender is Player) {
            if (!sender.hasPermission("papycraft.upr")) {
                sender.sendMessage("${ChatColor.RED}Missing permissions (papycraft.upr)")
                return true
            }

            val target = getTargetEntity(sender, ArmorStand::class.java, 16.toDouble())
            if (target == null) {
                sender.sendMessage("${ChatColor.RED}You're not looking at an armor stand")
                return true
            }
            when {
                command.name == "/gilet" -> applyGilet(target)
                command.name == "/crs" -> applyCRS(target)
                else -> sender.sendMessage("${ChatColor.RED}dafuk is dat command are u drunk") // :^)
            }
        } else
            sender.sendMessage("hello non-human thing, ur on a wild human zone here...")
        return true
    }

    private fun applyGilet(entity: LivingEntity) {
        val gilet = ItemStack(Material.GOLDEN_CHESTPLATE)
        val giletMeta = gilet.itemMeta
        giletMeta.displayName = "Gilet jaune"
        giletMeta.isUnbreakable = true
        giletMeta.addEnchant(Enchantment.BINDING_CURSE, 1, false)
        gilet.itemMeta = giletMeta

        val bleu = ItemStack(Material.LEATHER_LEGGINGS)
        val bleuMeta = bleu.itemMeta as LeatherArmorMeta
        bleuMeta.color = fromRGB(0x169C9C)
        bleuMeta.isUnbreakable = true
        bleu.itemMeta = bleuMeta

        val boots = ItemStack(Material.LEATHER_BOOTS)
        val bootsMeta = boots.itemMeta as LeatherArmorMeta
        bootsMeta.color = Color.BLACK
        bootsMeta.isUnbreakable = true
        boots.itemMeta = bootsMeta

        if (entity is Player) {
            if (entity.inventory.chestplate == null)
                entity.inventory.chestplate = gilet

            if (entity.inventory.leggings == null)
                entity.inventory.leggings = bleu

            if (entity.inventory.boots == null)
                entity.inventory.boots = boots
        } else if (entity is ArmorStand) {
            val tools = arrayListOf(
                    ItemStack(Material.IRON_PICKAXE),
                    ItemStack(Material.IRON_AXE),
                    ItemStack(Material.IRON_SHOVEL),
                    ItemStack(Material.IRON_HOE)
            )
            val items = arrayListOf(
                    ItemStack(Material.SIGN),
                    ItemStack(Material.TORCH),
                    ItemStack(Material.AIR),
                    ItemStack(Material.AIR)
            )
            bootsMeta.color = Color.BLACK
            bootsMeta.isUnbreakable = true
            boots.itemMeta = bootsMeta

            entity.helmet = ItemStack(Material.PLAYER_HEAD)
            entity.chestplate = gilet
            entity.leggings = bleu
            entity.boots = boots
            entity.isSmall = false
            entity.setArms(true)
            entity.setBasePlate(false)
            entity.equipment.itemInMainHand = tools.random()
            entity.equipment.itemInOffHand = items.random()
        }
    }

    private fun applyCRS(entity: ArmorStand) {
        val weapon = arrayListOf(
                ItemStack(Material.STONE_SWORD),
                ItemStack(Material.STICK),
                ItemStack(Material.TRIPWIRE_HOOK)
        )
        entity.helmet = ItemStack(Material.WITHER_SKELETON_SKULL)
        entity.chestplate = ItemStack(Material.CHAINMAIL_CHESTPLATE)
        entity.leggings = ItemStack(Material.CHAINMAIL_LEGGINGS)
        entity.boots = ItemStack(Material.CHAINMAIL_BOOTS)
        entity.isSmall = true
        entity.setArms(true)
        entity.setBasePlate(false)
        entity.equipment.itemInMainHand = weapon.random()
        entity.equipment.itemInOffHand = ItemStack(Material.SHIELD)
    }

    private fun <E : Entity> getTargetEntity(player: Player, clazz: Class<E>, range: Double): E? {
        val dir = player.location.direction
        val loc = player.eyeLocation.clone()
        while (player.eyeLocation.distanceSquared(loc) <= range * range) {
            if (loc.block.type.isOccluding) return null // Block is in the way
            for (entity in getNearbyEntities(clazz, loc, 1.25)) {
                if (entity !== player) return entity
            }
            loc.add(dir)
        }
        return null
    }

    private fun <E : Entity> getNearbyEntities(clazz: Class<E>, loc: Location, radius: Double): Set<E> {
        val tmp = HashSet<E>()
        val cRad = if (radius < 16) 1 else ((radius - radius % 16) / 16).toInt()
        for (x in -cRad until cRad) {
            for (z in -cRad until cRad) {
                for (entity in Location(loc.world, loc.x + x * 16, loc.y, loc.z + z * 16).chunk.entities) {
                    if (entity.location.distanceSquared(loc) <= radius * radius && clazz.isInstance(entity)) {
                        @Suppress("UNCHECKED_CAST")
                        tmp.add(entity as E)
                    }
                }
            }
        }
        return tmp
    }
}
